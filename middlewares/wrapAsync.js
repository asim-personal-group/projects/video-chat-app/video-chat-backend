module.exports = function (handler) {
	return async (req, res, next) => {
		try {
			return await handler(req, res, next);
		} catch (err) {
			console.error(err);
			return Promise.reject(err);
		}
	};
};
