const { getAllUsers, setUserSocket, getUserById, setUserRoom } = require("../storage").userDb;
const wrapper = require("../middlewares/wrapAsync");

module.exports = (io) => {
	const joinRoom = wrapper((roomId, userId) => {});

	const disconnect = wrapper(async (userId) => {
		await setUserSocket(userId, null);
		await setUserRoom(userId, "");
		let userCollection = await getAllUsers();
		io.sockets.emit("user-change", userCollection);
	});

	// from caller
	const callUser = wrapper(async (data) => {
		let { caller: callerId, callee: calleeId, signal } = data;
		let callerUser = await getUserById(callerId);
		let calleeUser = await getUserById(calleeId);
		if (callerId === calleeId) {
			io.to(callerUser.socket).emit("call-error", { error: "Cannot call yourself" });
		} else if (calleeUser.socket === null) {
			io.to(callerUser.socket).emit("call-error", { error: "User offline" });
		} else if (calleeUser.currentRoom !== "" && calleeUser.currentRoom !== null) {
			io.to(callerUser.socket).emit("call-error", { error: "User busy" });
			// request-to-join (broadcast to all members in the room)
		} else {
			io.to(calleeUser.socket).emit("incoming-call", { signal, caller: callerUser });
			await setUserRoom(callerId, "joined");
		}
	});

	// from caller
	const cancelCall = wrapper(async (data) => {
		let { callee: calleeId, caller: callerId } = data;
		let calleeUser = await getUserById(calleeId);
		io.to(calleeUser.socket).emit("cancelled");
		await setUserRoom(callerId, "");
		await setUserRoom(calleeId, "");
	});

	// from callee
	const acceptCall = wrapper(async (data) => {
		let { caller: callerId, signal, callee: calleeId } = data;
		let callerUser = await getUserById(callerId);
		io.to(callerUser.socket).emit("accepted", { signal });
		await setUserRoom(calleeId, "joined");
	});

	// from either caller or callee
	const endCall = wrapper(async (data) => {
		let { peer: peerId, user: userId } = data;
		let peerUser = await getUserById(peerId);
		io.to(peerUser.socket).emit("ended");
		await setUserRoom(peerId, "");
		await setUserRoom(userId, "");
	});

	// from callee
	const rejectCall = wrapper(async (data) => {
		let { caller: callerId } = data;
		let callerUser = await getUserById(callerId);
		io.to(callerUser.socket).emit("rejected");
		await setUserRoom(callerId, "");
	});

	io.on("connection", async (socket) => {
		let userId = socket.handshake.auth.id;
		await setUserSocket(userId, socket.id);
		let userCollection = await getAllUsers();
		io.sockets.emit("user-change", userCollection);

		socket.on("disconnect", () => disconnect(userId));

		socket.on("call-user", callUser);

		socket.on("cancel-call", cancelCall);

		socket.on("accept-call", acceptCall);

		socket.on("end-call", endCall);

		socket.on("reject-call", rejectCall);

		socket.on("join-room", joinRoom);
		/*
		socket.on("allow-user", (data) => {

		})
		// */
	});
};
