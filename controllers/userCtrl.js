const userDb = require("../storage/userDb");
const wrapper = require("../middlewares/wrapAsync");
const { createUser, getUserById, getAllUsers } = userDb;

const userCtrl = {
	login: wrapper(async (req, res) => {
		let { username } = req.body;
		if (!username || username.length < 1) return res.status(406).send({ error: "Username is required" });
		let user = await getUserById(username);
		if (!user) user = await createUser(username);
		return res.status(200).send(user);
	}),

	listUsers: wrapper(async (req, res) => {
		let users = await getAllUsers();
		res.status(200).send({ count: users.count, users });
	}),
};

module.exports = userCtrl;
