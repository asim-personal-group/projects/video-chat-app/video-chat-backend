//  CONFIG
require("dotenv").config();

//  PACKAGES
const express = require("express");
const cors = require("cors");
const socketio = require("socket.io"); // Server
const path = require("path");
const https = require("https");
const fs = require("fs");

//  IMPORTS
const connectDb = require("./storage/config");
const ioHandler = require("./socket");
const userRoutes = require("./routes/userRoutes");

const isProduction = process.env.NODE_ENV === "production";
const port = process.env.PORT || 4357;
const isHttps = port === 443;
let io;
let server;

const initializeSocket = () => {
	io = socketio(server, { cors: { origin: "*" } }); // instance of Server -> server
	ioHandler(io);
};

//  APP START
connectDb();
const app = express();
app.use(cors({ origin: "*" }));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use("/user", userRoutes);

app.get("/health", (req, res) => {
	return res.status(200).send({
		status: "LIVE",
		time: new Date(),
	});
});

if (isProduction) {
	app.use(express.static(path.join(__dirname, "./public")));
	app.get("*", (req, res) => {
		return res.sendFile(path.join(__dirname, "./public/index.html"));
	});
}

process.on("unhandledRejection", (reason, p) => console.error(`Unhandled Rejection at: ${p}, ${reason}`));

process.on("uncaughtException", function (err) {
	console.error(err);
	console.error(err.stack);
});

if (isProduction && isHttps) {
	const key = fs.readFileSync(process.env.SSL_KEY);
	const cert = fs.readFileSync(process.env.SSL_CERT);
	server = https.createServer({ key, cert }, app);
	server.listen(port, () => initializeSocket());
} else {
	server = app.listen(port, () => initializeSocket());
}
