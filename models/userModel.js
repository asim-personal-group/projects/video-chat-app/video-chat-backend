const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const UserSchema = new Schema({
	id: { type: String, required: true, unique: true },
	username: { type: String, required: true, minLength: 3 },
	socket: { type: String, default: "" },
	currentRoom: { type: String, default: "" },
});
UserSchema.index("id");

module.exports = mongoose.model("User", UserSchema);
