const express = require("express");

const userCtrl = require("../controllers").userCtrl;

const router = express.Router();

router.post("/login", userCtrl.login);
router.get("/list", userCtrl.listUsers);

module.exports = router;
