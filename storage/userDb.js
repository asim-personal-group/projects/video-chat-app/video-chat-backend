const shortId = require("shortid");
const User = require("../models/userModel");

const createUser = async (username) => {
	try {
		let match = true;
		let id;
		while (match) {
			id = shortId.generate();
			let user = await getUserById(id);
			if (!user) match = false;
		}
		let user = new User({
			id,
			username,
		});
		await user.save();
		return user;
	} catch (err) {
		throw err;
	}
};

const getAllUsers = async () => {
	try {
		let users = {};
		let userList = await User.find().lean();
		for (const user of userList) {
			users[user.id] = user;
		}
		return users;
	} catch (err) {
		throw err;
	}
};

const getUserById = async (userId) => {
	try {
		return await User.findOne({ id: userId }).lean();
	} catch (err) {
		throw err;
	}
};

const setUserSocket = async (userId, socketId) => {
	return await User.findOneAndUpdate({ id: userId }, { socket: socketId }, { new: true, lean: true });
};

const setUserRoom = async (userId, roomId) => {
	return await User.findOneAndUpdate({ id: userId }, { currentRoom: roomId }, { new: true, lean: true });
};

module.exports = {
	createUser,
	getAllUsers,
	getUserById,
	setUserSocket,
	setUserRoom,
};
