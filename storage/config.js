const mongoose = require("mongoose");

const DB_PW = "NYvrqoMale2NZG39";
const DB_URI = `mongodb+srv://mohammad-asim-iqbal:${DB_PW}@video-chat-cluster-0.fgjhj.mongodb.net/video-chat-app?retryWrites=true&w=majority`;
const DB_OPTIONS = {
	keepAlive: 1,
	connectTimeoutMS: 30000,
	useNewUrlParser: true,
	useUnifiedTopology: true,
	useFindAndModify: false,
	useCreateIndex: true,
};

async function connectDb() {
	mongoose.set("debug", process.env.NODE_ENV === "development");
	mongoose.connect(DB_URI, DB_OPTIONS);
	mongoose.connection.on("connected", function () {
		console.info(`Database connected`);
	});
	mongoose.connection.on("error", function (err) {
		console.error("Mongoose default connection has received " + err + " error");
	});
	mongoose.connection.on("disconnected", function () {
		console.info("Mongoose default connection is disconnected");
	});
	process.on("SIGINT", function () {
		mongoose.connection.close(function () {
			console.info("Mongoose default connection is disconnected due to application termination");
			process.exit(0);
		});
	});
}

module.exports = connectDb;
